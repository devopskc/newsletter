# Tech Pause Newsletter

This newsletter uses mjml. The plan is to convert that to html and send it using
Lyris. We will also drop a pdf in [the `W` drive.](W:\Cloud Program\Newsletters)

## Pre-requisites

Install [node](//nodejs.org/en/download/).

## Create the html

You'll first need to install mjml:

```bash
npm install mjml
```

To create the html you need to enter this command:

```bash
mjml -r newsletters/<input_filename.mjml> -o newsletters/<output_filename.html>
```

## Create a pdf with proper rendering

You'll first need to install puppeteer:

```bash
npm install puppeteer
```

To create the pdf you need to enter this command:

```bash
node html-to-pdf.js
```

This currently goes through each html file in `newsletters` and creates a pdf.
It's idempotent, so it doesn't matter if a pdf already exists. That won't cause
it to fail.
