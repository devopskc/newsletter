'use strict';

var fs = require('fs');
var path = require('path');
const puppeteer = require('puppeteer');

var walkPath = './public/newsletters';

var walk = function (dir, done) {
    fs.readdir(dir, function (error, list) {
        if (error) {
            return done(error);
        }

        var i = 0;

        (function next () {
            var file = list[i++];

            if (!file) {
                return done(null);
            }
            
            file = dir + '/' + file;
            
            fs.stat(file, function (error, stat) {
        
                if (stat && stat.isDirectory()) {
                    walk(file, function (error) {
                        next();
                    });
                } else {
                    if (path.extname(file) == '.html') {
                        var base = file.replace('.html', '');
                        var name = path.basename(file, path.extname(file));
                        (async() => {    
                            const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']});
                            const page = await browser.newPage();
                            await page.goto(`file:${path.join(__dirname, base.concat('.html'))}`, {waitUntil: 'load'});
                            await page.emulateMedia('screen');
                            await page.pdf({
                              path: 'public/pdfs/' + name + '.pdf',
                              format: 'letter',
                              margin: {
                                    top: "20px",
                                    left: "20px",
                                    right: "20px",
                                    bottom: "20px"
                              }    
                            });
                            await browser.close();
                        })();
                    }
                    next();
                }
            });
        })();
    });
};

walk(walkPath, function(error) {
    if (error) {
        throw error;
    }
});
